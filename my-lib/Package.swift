// swift-tools-version:4.0
import PackageDescription


let package = Package(
    name: "my-lib-package",
    products: [
        .library(name: "my-lib", targets: ["my-lib"]),
        //.library(name: "my-lib", type: .static, targets: ["my-lib"]),
        //.library(name: "my-lib", type: .dynamic, targets: ["my-lib"]),
    ],
    dependencies: [
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "my-lib",
            dependencies: []),
        .testTarget(
            name: "my-libTests",
            dependencies: ["my-lib"]),
    ]
)
